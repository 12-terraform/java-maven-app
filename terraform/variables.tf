variable vpc_cidr_block {
    default = "10.0.0.0/16"
}
variable subnet_cidr_block {
    default = "10.0.10.0/24"
}
variable avail_zone {
    default = "ca-central-1c"
}
variable env_prefix {
    default = "dev"
}
variable my_ip {
    default = "142.169.16.132/32"
}
variable jenkins_ip {
    default = "143.110.217.45/32"
}
variable instance_type {
    default = "t2.micro"
}
variable region {
    default = "ca-central-1"
}