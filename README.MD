### Demo Project:
Complete CI/CD with Terraform

### Technologies used:
Terraform, Jenkins, Docker, AWS, Git, Java, Maven, Linux, Docker Hub

### Project Description:
Integrate provisioning stage into complete CI/CD Pipeline to automate provisioning server instead of deploying to an existing server
- Create SSH Key Pair
    - install ssh agent in Jenkins UI
    - configure ssh credentials in Jenkins UI
- Install Terraform inside Jenkins container
    - use cat /etc/os-release to check OS then install Terraform accordingly
    - to add Terraform Linux repo - apt install software-properties-common - 
    - then use apt-add-repository to add repo
    - then install Terraform
- Add Terraform configuration to application’s git repository
- Adjust Jenkinsfile to add “provision” step to the CI/CD pipeline that provisions EC2 instance
- So the complete CI/CD project we build has the following configuration:
    1. CI step: Build artifact for Java Maven application
    2. CI step: Build and push Docker image to Docker Hub
       - Authenticate with DockerHub using credentials configured in Jenkins pipeline
    3. CD step: Automatically provision EC2 instance using TF
        - Get AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY from Jenkins, where they were set, using credentials e.g credentials('jenkins_aws_secret_access_key')
        - Get EC2 server ip dynamically from Terraform output and store it in a variable
    4. CD step: Deploy new application version on the provisioned EC2 instance with Docker Compose
        - Get AWS credentials from Jenkins, for use in EC2 server, using credentials e.g. DOCKER_CREDS = credentials('docker-hub-repo') then user=DOCKER_CREDS_USR and password=DOCKER_CREDS_PSW
        - Pass aws credentials to shell commands file on EC2 like this "bash ./server-cmds.sh ${IMAGE_NAME} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"